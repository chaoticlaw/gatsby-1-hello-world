import React from "react";

class Counter extends React.Component {
  constructor() {
    super();
    this.state = { count: 0 }
  }

  render() {
    return (
      <div>
        <h1>Counter</h1>
        <p>Current Count: {this.state.count}</p>
        <div>
          <button onClick={() => this.setState({ count: this.state.count + 1})}>Add</button>&nbsp;
          <button onClick={() => this.setState({ count: this.state.count - 1})}>Minus</button>
        </div>
      </div>
    )
  }
}

export default Counter;
