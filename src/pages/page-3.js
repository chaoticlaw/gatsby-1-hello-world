import React from "react";
import Link from "gatsby-link";

export default () =>
  <div>
    <h1>Heading for page 3 here!</h1>
    <p><Link to="/">Go back</Link></p>
  </div>
