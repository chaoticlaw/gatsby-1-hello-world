import React from "react";
import Link from "gatsby-link";

export default () =>
  <div style={{ color: `tomato` }}>
    <h1>Hello Gatsby!</h1>
    <p>This is a paragraph</p>
    <figure>
    <img src="https://source.unsplash.com/random/400x200" alt="A random image, 400 pixels wide by 200 high" />
    <figcaption>
      A random image!
    </figcaption>
    </figure>
    <ul>
      <li><Link to="/page-2/">Page 2</Link></li>
      <li><Link to="/page-3/">Go to Page 3</Link></li>
      <li><Link to="/counter/">Let’s count!</Link></li>
    </ul>
  </div>
