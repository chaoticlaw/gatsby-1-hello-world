import React from "react";
import Link from "gatsby-link";

export default () =>
  <div>
    <p>Hello from the second Gatsby page</p>
    <p><Link to="/">Return Home</Link></p>
  </div>
